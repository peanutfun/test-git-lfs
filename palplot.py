import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

sns.set()
sns.palplot(sns.color_palette())
plt.savefig("plot.pdf")